package asing;

import java.util.Scanner;

public class Exp3
{
	
	public static void checkpar(String ex,int size)
	{
		int i=0,copen=0,cclosed=0;
		while(i<size)
		{
			if(ex.charAt(i)=='(')
			{
				copen++;
			}
			if(ex.charAt(i)==')')
			{
				cclosed++;
			}
			i++;
		}
		if(copen==cclosed)
		{
			System.out.println("Balanced");
		}
		else {
			System.out.println("Not Balanced");
		}
	}
	
	public static void main(String args[])
	{
		String ex;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter expression");
		ex=sc.next();
		int size=ex.length();
		checkpar(ex,size);		
	}
}
