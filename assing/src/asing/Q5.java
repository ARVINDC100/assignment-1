package asing;

class Student
{
	static Student obj=null;
	private Student()
	{
		System.out.println("Default constructor");
	}

	public static Student getObj()
	{
		if(obj==null)
		{
			obj=new Student();
		}
		return obj;
	}
}
public class Q5 {
	public static void main(String args[])
	{
		Student obj=Student.getObj();
		Student obj2=Student.getObj();
		System.out.println(obj.hashCode());
		System.out.println(obj2.hashCode());
	}
}
